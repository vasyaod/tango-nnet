#!/usr/bin/python

import tensorflow as tf
import numpy as np
import math
import argparse

from graph_v5_without_depth_4_cascades import *
from datasethttp import *

parser = argparse.ArgumentParser()
parser.add_argument('-b', '--butch-size', action='store', type=int, required=True)
parser.add_argument('-o', '--number-of-objects', action='store', type=int, required=True)
parser.add_argument('-p', '--project-id', action='store', type=str, required=True)
parser.add_argument('-u', '--generator-url', action='store', type=str, required=True)
parser.add_argument('-e', '--epochs', action='store', type=int, required=True)
args = parser.parse_args()

batch_size = args.butch_size
objects = args.number_of_objects
project_id = args.project_id

def class_to_coordinates(data, coord):
    x_res = (1920.0 / 5.0 / 4.0)
    y_res = (1080.0 / 5.0 / 4.0)
    
    for i in range(np.shape(data)[1]):
        z = np.argmax(data[0][i])
        if z == 0:
            x = 0
            y = 0
        else:
            x = (z - 1) % int(math.ceil(x_res))
            y = (z - 1) // int(math.ceil(x_res))

        print(step, "1--> ", np.shape(data), z, x, y, coord[0][i], float(x) / x_res , float(y) / y_res)

test_set_image_inp, _, test_set_class1, test_set_class2, test_set_class3, _ = get_banch(project_id, objects, batch_size, args.generator_url, True)
single_item_image_inp1, _, _, _, _, single_item_coord1 = get_banch(project_id, objects, 1, args.generator_url)
single_item_image_inp2, _, _, _, _, single_item_coord2 = get_banch(project_id, objects, 1, args.generator_url)
single_item_image_inp3, _, _, _, _, single_item_coord3 = get_banch(project_id, objects, 1, args.generator_url)

def classes_calc(input, stride, height, wight):
    bunch_size = tf.shape(input)[0]
    objects = tf.shape(input)[1]
    x = input[:,:,1:]
    x = tf.reshape(x, [bunch_size * objects, height, wight, 1])

    h = int(math.ceil(height / float(stride)))
    w = int(math.ceil(wight / float(stride)))

    conv_matrix = tf.ones([stride, stride, 1, 1], tf.float32)
    x = tf.nn.conv2d(x, filter=conv_matrix, strides=[1, stride, stride, 1], padding="SAME")
    x = tf.reshape(x, [bunch_size, objects, h * w])
    x = tf.concat([input[:,:,0:1], x], 2)
    return x

out_classes1 = tf.placeholder(tf.float32)
out_classes2 = classes_calc(out_classes1, 4, 54, 96)
out_classes3 = classes_calc(out_classes2, 4, 14, 24)
out_classes4 = classes_calc(out_classes3, 2, 4, 6)

p_keep_conv = 1.0
wight = 1920 / 5 #384
height = 1080 / 5 #216

def init_weights(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype = tf.float32), name = name)

def init_weights2(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.03, dtype = tf.float32), name = name)

image_input, z4, z3, z2, z1 = create_graph(wight, height, objects, p_keep_conv, init_weights, init_weights2)

#################################################################################
# Cost
#
z1_cost = tf.reduce_sum(out_classes1 * tf.log(z1 + 1e-10), axis=[1, 2]) / objects
z2_cost = tf.reduce_sum(out_classes2 * tf.log(z2 + 1e-10), axis=[1, 2]) / objects
z3_cost = tf.reduce_sum(out_classes3 * tf.log(z3 + 1e-10), axis=[1, 2]) / objects
z4_cost = tf.reduce_sum(out_classes4 * tf.log(z4 + 1e-10), axis=[1, 2]) / objects

z1_cost_mean = -tf.reduce_mean(z1_cost, name = "cost-z1")
z2_cost_mean = -tf.reduce_mean(z2_cost, name = "cost-z2")
z3_cost_mean = -tf.reduce_mean(z3_cost, name = "cost-z3")
z4_cost_mean = -tf.reduce_mean(z4_cost, name = "cost-z4")

cost = z1_cost_mean + (z2_cost_mean * z2_cost_mean) + (z3_cost_mean * z3_cost_mean * z3_cost_mean) + (z4_cost_mean * z4_cost_mean * z4_cost_mean * z4_cost_mean)

#cost = -tf.reduce_sum((out_data * tf.log(tf.clip_by_value(z1,1e-10,1.0))), name = "cost")
#cost = -tf.reduce_sum((out_data * tf.log(z1 + 1e-10)), name = "cost")
#cost = -tf.reduce_sum((out_data * tf.log(z1))) + tf.reduce_sum((z1 * tf.log(z1)))
#cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(z3, out_data))
#cost = tf.reduce_sum(tf.pow(z1 - out_data, 2))
#train_op = tf.train.RMSPropOptimizer(0.001, 0.9).minimize(cost)
#train_op = tf.train.GradientDescentOptimizer(0.005).minimize(cost)
train_op = tf.train.AdamOptimizer(0.001).minimize(cost)

# Initilizing the variables
init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 1 }
    )
saver = tf.train.Saver()

with tf.Session(config=config) as sess:
    sess.run(init)
    #saver.restore(sess, 'model/my-model')
    for step in range(args.epochs):

        image_inp, _, out1, out2, out3, _ = get_banch(project_id, objects, batch_size, args.generator_url)

        tm = current_milli_time()
        _, m1, m2, m3, m4, m5 = sess.run([train_op, cost, z1_cost_mean, z2_cost_mean, z3_cost_mean, z4_cost_mean], feed_dict={image_input: image_inp, out_classes1: out1})
#        print("Proccessing time", current_milli_time() - tm)

        if step % 1 == 0:
            print(step, m1, m2, m3, m4, m5)

        if step % 10 == 0:
            cost_test = sess.run(cost, feed_dict={image_input: test_set_image_inp, out_classes1: test_set_class1})
            print(step, "Test cost: ", cost_test)

            class_to_coordinates(
                sess.run(z1, feed_dict={image_input: single_item_image_inp1}),
                single_item_coord1
            )
            class_to_coordinates(
                sess.run(z1, feed_dict={image_input: single_item_image_inp2}),
                single_item_coord2
            )
            class_to_coordinates(
                sess.run(z1, feed_dict={image_input: single_item_image_inp3}),
                single_item_coord3
            )

        if step % 100 == 0 and step > 1:
            tf.train.write_graph(sess.graph, 'model', 'train.pb', as_text=False)
            saver.save(sess, 'model/my-model')
#!/usr/bin/env python

from random import randint
import numpy as np
from datasethttp import *

x1, x2, x3, x4, x5, x6 = get_banch("test", 1, 1)

assert(x1.item((0, 0, 0, 0)) == 0)
assert(x1.item((0, 0, 1, 0)) == 1)
assert(x1.item((0, 0, 2, 0)) == 2)
assert(x1.item((0, 0, 3, 0)) == 3)

assert(x1.item((0, 1, 0, 0)) == 384)
assert(x1.item((0, 1, 1, 0)) == 385)
assert(x1.item((0, 1, 2, 0)) == 386)
assert(x1.item((0, 1, 3, 0)) == 387)

assert(x2.item((0, 0, 0, 0)) == 0)
assert(x2.item((0, 0, 1, 0)) == 1)
assert(x2.item((0, 0, 2, 0)) == 2)
assert(x2.item((0, 0, 3, 0)) == 3)

assert(x2.item((0, 1, 0, 0)) == 384)
assert(x2.item((0, 1, 1, 0)) == 385)
assert(x2.item((0, 1, 2, 0)) == 386)
assert(x2.item((0, 1, 3, 0)) == 387)

assert(x6[0, 0, 0] == 0.5)

get_banch("two-fingers", 2, 35)
#!/usr/bin/python

import tensorflow as tf
import numpy as np
import glob
import random

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

def save_parameters(sess, par_names):
    for par_name in par_names:
        data = sess.run(par_name + ":0")
        print(par_name, np.sum(data))

        f = open("model/" + par_name,'wb')
        x = np.float32(data)
        f.write(x)
        f.close()
        #data.tofile()
        data_shape = []
        data_shape.append(data.shape)
        np.array(data_shape).tofile("model/" + par_name + ".shape", sep = " ")


with tf.Session(config=config) as sess:
    saver = tf.train.import_meta_graph('model/my-model.meta')
    saver.restore(sess, 'model/my-model')
    #x1, x2, _, _, _ = read_file("output/1484465778790")
#    w = sess.run("int-w3:0", feed_dict = {"input:0": x2})
#    w = sess.run("int-w3")

    varss = []
    for n in tf.global_variables():
        if "/" not in n.name and "beta" not in n.name:
           varss.append(n.name.replace(":0",""))
    
    varsfile = open('model/variables', 'w')
    for v in varss:
        varsfile.write("%s\n" % v)

    save_parameters(sess, varss)

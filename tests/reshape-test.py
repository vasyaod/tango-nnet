#!/usr/bin/python

import tensorflow as tf
import numpy as np
import math

v0 = np.array([[[[0,20], [1,2], [2,3], [3,4]], 
                [[4,5], [5,6], [6,7], [7,8]],
                [[8,9], [9,10], [10,11], [11,12]],
                [[12,13], [13,14], [14,15], [15,16]]
               ],
               [
                [[0,20], [1,2], [2,3], [3,4]], 
                [[4,5], [5,6], [6,7], [7,8]],
                [[8,9], [9,10], [10,11], [11,12]],
                [[12,13], [13,14], [14,15], [15,16]]]
               ])

inp_data = tf.placeholder(tf.float32)
#y = tf.pad(inp_data, [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT")

def init_weights(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype = tf.float32), name = name)
y = inp_data
y = tf.extract_image_patches(inp_data, [1, 3, 3, 1], [1, 1, 1, 1], [1, 1, 1, 1], padding='VALID')
#w = tf.zeros([16, 18, 3])
#b = tf.zeros([16, 3])
#y = tf.einsum("ijp,jpr->ijr", y, w) + b
#y = y + b
#y = tf.tensordot(y, w, 1)
#y = tf.reshape(y, [16, -1])
#y = tf.concat([y, y], 1)
#y = y[:,1:]
    
#y = patches_to_image(inp_data, 4, 4, 2, 2)
#y = tf.reshape(inp_data, [2, 2, 2, 2])
#y = tf.reshape(y, [4, 2, 2])

init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1 = sess.run(y, feed_dict={inp_data: v0})
    print(np.shape(o1))
    print(o1)

#!/usr/bin/python

from random import randint
import numpy as np
import filereading

x1, x2, x3, x4, x5 = read_file("output-test/1")

#inp = np.array(np.loadtxt("output-test/1.image"))
#print np.expand_dims(inp, axis=0)

assert np.array_equal(x1, np.array(
        [[
            [[2.0], [0.0], [0.0], [0.0]],
            [[0.0], [2.0], [0.0], [0.0]]
        ]]
    ))

assert np.array_equal(x2, np.array(
        [[
            [[1.0], [0.0], [0.0], [0.0]],
            [[0.0], [1.0], [0.0], [0.0]]
        ]]
    ))

assert np.array_equal(x3, np.array(
        [[0.0, 0.0, 0.0, 0.0]]
    ))

assert np.array_equal(x5, np.array(
        [[
            [[2.0, 1.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
            [[0.0, 0.0], [2.0, 1.0], [0.0, 0.0], [0.0, 0.0]]
        ]]
    ))


x1, x2, x3, x4, x5 = get_banch(["output-test/1", "output-test/2"])

assert np.array_equal(x1, np.array(
        [
            [
                [[2.0], [0.0], [0.0], [0.0]],
                [[0.0], [2.0], [0.0], [0.0]]
            ],
            [
                [[3.0], [0.0], [0.0], [0.0]],
                [[0.0], [3.0], [0.0], [0.0]]
            ]
        ]
    ))

assert np.array_equal(x2, np.array(
        [
            [
                [[1.0], [0.0], [0.0], [0.0]],
                [[0.0], [1.0], [0.0], [0.0]]
            ],
            [
                [[2.0], [0.0], [0.0], [0.0]],
                [[0.0], [2.0], [0.0], [0.0]]
            ]
        ]
    ))

assert np.array_equal(x3, np.array(
        [[0.0, 0.0, 0.0, 0.0],
         [1.0, 0.0, 3.0, 1.0]]
    ))

assert np.array_equal(x4, np.array(
        [[0.1, 0.2],
         [0.4, 0.5]]
    ))

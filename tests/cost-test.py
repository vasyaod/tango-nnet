#!/usr/bin/python

import tensorflow as tf
import numpy as np

v0 = np.array([[1, 2, 3],[2, 2, 3]])

inp_data = tf.placeholder(tf.float32)
y = tf.reduce_sum(tf.pow(inp_data, 2))

init = tf.initialize_all_variables()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1 = sess.run([y], feed_dict={inp_data: v0})
    print(o1)

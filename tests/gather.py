#!/usr/bin/python

import tensorflow as tf
import numpy as np

c = tf.constant([[[[1,6,2],
                   [3,4,5]],
                  [[1,6,2],
                   [3,4,5]]]])
c1 = tf.constant([[[1,2],[1,2]]])
c2 = tf.constant([[[1,6,2,0],
                   [3,4,5,0]],
                  [[7,6,2,0],
                   [3,4,5,0]],
                  [[7,6,2,0],
                   [3,4,5,0]],
                  [[7,6,9,0],
                   [3,4,5,0]]])
c3 = tf.constant([[[1,0],[6,0],[2,0]],
                  [[3,0],[4,0],[5,0]],
                  [[7,8],[9,6],[1,4]]])

x = tf.gather(c3, [1,0, 1, 1 ,1])


# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    o1 = sess.run(x)
    print(o1)
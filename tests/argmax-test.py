#!/usr/bin/python

import tensorflow as tf
import numpy as np

c = tf.constant([[[[1,6,2],
                   [3,4,5]],
                  [[1,6,2],
                   [3,4,5]]]])
c1 = tf.constant([[[1,2],[1,2]]])
c2 = tf.constant([[[1,6,2,0],
                   [3,4,5,0]],
                  [[7,6,2,0],
                   [3,4,5,0]],
                  [[7,6,2,0],
                   [3,4,5,0]],
                  [[7,6,9,0],
                   [3,4,5,0]]])
c3 = tf.constant([[[1,0],[6,0],[2,0]],
                  [[3,0],[4,0],[5,0]]])
bunch_size = 4
objects = 2
top = 2

index_part1 = tf.nn.top_k(c2,top)[1]
index_part1 = tf.reshape(index_part1, [bunch_size * objects * top])

index_part2 = tf.range(bunch_size)
index_part2 = tf.reshape(index_part2, [bunch_size,1])
index_part2 = tf.tile(index_part2, [1, objects * top])
index_part2 = tf.reshape(index_part2, [bunch_size * objects * top])

index_part3 = tf.range(objects)
index_part3 = tf.reshape(index_part3, [objects,1])
index_part3 = tf.tile(index_part3, [1, top])
index_part3 = tf.reshape(index_part3, [objects * top])
index_part3 = tf.tile(index_part3, [bunch_size])

indecies_type1 = tf.stack([index_part2, index_part3, index_part1])
indecies_type1 = tf.transpose(indecies_type1)
indecies_type2 = tf.stack([index_part2, index_part1])
indecies_type2 = tf.transpose(indecies_type2)

# r = tf.stack([tf.range(2, dtype=tf.int64), y])
# r = tf.transpose(r)
#y = tf.gather_nd(c2,y)

#flat_y = tf.reshape(c3, [-1, 2])
#flat_ind_max = y + tf.cast(tf.range(tf.shape(c2)[0]) * tf.shape(c2)[1], tf.int64)
#y_ = tf.gather(flat_y, flat_ind_max)
y_ = tf.gather_nd(c2, indecies_type1)

scatter= tf.scatter_nd(indecies_type1, y_, [bunch_size, objects, 4])
# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
#    o1 = sess.run(y)
#    print(o1)

    o1 = sess.run(indecies_type1)
    print(o1)

    o1 = sess.run(indecies_type2)
    print(o1)
    
    o1 = sess.run(y_)
    print(o1)

    o1 = sess.run(scatter)
    print(o1)
#!/usr/bin/python

import tensorflow as tf
import numpy as np
import math
c1 = tf.constant([[[100,1,2,3,4,5,6]], [[101,2,2,3,4,5,6]]], tf.float32)
def classes_calc(input, stride, height, wight):
    bunch_size = tf.shape(input)[0]
    objects = tf.shape(input)[1]
    x = input[:,:,1:]
    x = tf.reshape(x, [bunch_size * objects, height, wight, 1])

    h = int(math.ceil(height / float(stride)))
    w = int(math.ceil(wight / float(stride)))

    conv_matrix = tf.ones([stride, stride, 1, 1], tf.float32)
    x = tf.nn.conv2d(x, filter=conv_matrix, strides=[1, stride, stride, 1], padding="SAME")
    x = tf.reshape(x, [bunch_size, objects, h * w])
    x = tf.concat([input[:,:,0:1], x], 2)
    return x

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    o1 = sess.run(classes_calc(c1, 2, 2, 3))
    print(o1)
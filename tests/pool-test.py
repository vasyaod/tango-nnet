#!/usr/bin/python

import tensorflow as tf
import numpy as np

v0 = np.array([[[[1,2],[2,3],[3,4],[5,6]], [[2,2],[2,3],[3,4],[5,6]]]])

inp_data = tf.placeholder(tf.float32)
y = tf.nn.avg_pool(inp_data, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

init = tf.initialize_all_variables()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 1 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1 = sess.run([y], feed_dict={inp_data: v0})
    print(o1)

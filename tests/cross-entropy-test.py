#!/usr/bin/python

import tensorflow as tf
import numpy as np

#v1 = np.array([[0.6, 0.2, 0.2]])
#v0 = np.array([[0.6, 0.3, 0.1]])

# Ideal state
v0 = np.array([[1, 0, 0]])
v1 = np.array([[0.25, 0.25, 0.25, 0.25]])

#v1 = np.array([[0.1, 0.9, 0]])
#v0 = np.array([[0.1, 0.9, 0]])

inp_data0 = tf.placeholder(tf.float32)
inp_data1 = tf.placeholder(tf.float32)

y = -tf.reduce_sum(inp_data0 * tf.log(inp_data1) + (1 - inp_data0) * tf.log(1 - inp_data1))

init = tf.initialize_all_variables()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1 = sess.run([y], feed_dict={inp_data0: v1, inp_data1: v1})
    print(o1)

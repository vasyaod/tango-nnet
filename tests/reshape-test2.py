#!/usr/bin/python

import tensorflow as tf
import numpy as np
import math

v0 = np.array([[
                [[0], [1], [2], [3]],
                [[4], [5], [6], [7]],
                [[8], [9], [10], [11]],
                [[12], [13], [14], [15]],
                [[16], [17], [18], [19]]
               ],
               [
                [[0], [1], [2], [3]],
                [[4], [5], [6], [7]],
                [[8], [9], [10], [11]],
                [[12], [13], [14], [15]],
                [[16], [17], [18], [19]]
               ]])

inp_data = tf.placeholder(tf.float32)
#y = tf.pad(inp_data, [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT")

def init_weights(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype = tf.float32), name = name)
y = inp_data
y = tf.extract_image_patches(inp_data, [1, 2, 2, 1], [1, 2, 2, 1], [1, 1, 1, 1], padding='SAME')

#y = tf.reshape(y, [2, 3, 2, 2, 2])
#y = tf.transpose(y, [0, 1, 3, 2, 4])
#y = tf.reshape(y, [2, 6 * 4])
#y = tf.reshape(y, [2, 6 * 4])

init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1 = sess.run(y, feed_dict={inp_data: v0})
    print(np.shape(o1))
    print(o1)

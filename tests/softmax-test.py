#!/usr/bin/python

import tensorflow as tf
import numpy as np

v0 = np.array([[0, 0, 0, 0, 0, 0, 1]])

inp_data = tf.placeholder(tf.float32)
y = tf.nn.softmax(inp_data)
y1 = tf.reduce_sum(y)

init = tf.initialize_all_variables()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 }
    )

with tf.Session(config=config) as sess:
    sess.run(init)
    o1, o2 = sess.run([y, y1], feed_dict={inp_data: v0})
    print(o1, o2)

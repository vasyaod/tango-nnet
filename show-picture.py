#!/usr/bin/python

import tensorflow as tf
import numpy as np
import glob
import random
import os
from filereading import *
import math
from PIL import Image
from scipy.misc import toimage
import matplotlib.pyplot as plt

image_input = tf.placeholder(tf.float32, name = "image_input")
depth_input = tf.placeholder(tf.float32, name = "depth_input")
out_classes1 = tf.placeholder(tf.float32)
out_classes2 = tf.placeholder(tf.float32)
out_classes3 = tf.placeholder(tf.float32)

def init_weights(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype = tf.float32), name = name)

def init_weights2(shape, name):
    return tf.Variable(tf.random_normal(shape, stddev=0.03, dtype = tf.float32), name = name)

p_keep_conv = 1.0
wight = 1920 / 5 #384
height = 1080 / 5 #216

################################################################
# Sobel filtering
#
sobel_x = tf.constant([[-1.0, 0.0, 1.0], [-2.0, 0.0, 2.0], [-1.0, 0.0, 1.0]], tf.float32)
sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])

# Sobel filter for image 
image_filtered_x = tf.nn.conv2d(image_input, sobel_x_filter,
                          strides=[1, 1, 1, 1], padding='SAME')
image_filtered_y = tf.nn.conv2d(image_input, sobel_y_filter,
                          strides=[1, 1, 1, 1], padding='SAME')

image_sobel_filtered = tf.sqrt(tf.square(image_filtered_x) + tf.square(image_filtered_y), name = "image_sobel_filtered")

# Sobel image for depth
depth_filtered_x = tf.nn.conv2d(depth_input, sobel_x_filter,
                          strides=[1, 1, 1, 1], padding='SAME')
depth_filtered_y = tf.nn.conv2d(depth_input, sobel_y_filter,
                          strides=[1, 1, 1, 1], padding='SAME')

depth_sobel_filtered = tf.sqrt(tf.square(depth_filtered_x) + tf.square(depth_filtered_y), name = "depth_sobel_filtered")

################################################################
# Convolution layer parameters
#
layer0_devider = 5
layer0_wight = int(math.ceil(1920 / layer0_devider))
layer0_height = int(math.ceil(1080 / layer0_devider))
layer0_size = layer0_wight * layer0_height
layer0_depth_size = 1

layer1_devider = 5 * 4
layer1_wight = int(math.ceil(1920 / layer1_devider))
layer1_height = int(math.ceil(1080 / layer1_devider))
layer1_size = layer1_wight * layer1_height
layer1_depth_size = 10

layer2_devider = 5 * 4 * 4
layer2_wight = 24 # int(math.ceil(1920 / layer2_devider))
layer2_height = 14 # int(math.ceil(1080 / layer2_devider))
layer2_size = layer1_wight * layer1_height
layer2_depth_size = 40

layer3_devider = 5 * 4 * 4 * 4
layer3_wight = 6 # int(math.ceil(1920 / layer3_devider))
layer3_height = 4 # int(math.ceil(1080 / layer3_devider))
layer3_size = layer1_wight * layer1_height
layer3_depth_size = 80

################################################################
# Convolution defenition
#
def conv_layer(input_data, input_depth_size, output_depth_size, name):
    w = init_weights2([4, 4, input_depth_size, output_depth_size], name)
    y = tf.nn.relu(tf.nn.conv2d(input_data, w, strides=[1, 1, 1, 1], padding='SAME'))
    y = tf.nn.max_pool(y, ksize=[1, 4, 4, 1], strides=[1, 4, 4, 1], padding='SAME')
    #y = tf.nn.dropout(0, p_keep_conv)
    return y

################################################################
# Image convolution layers
#
image_y0 = conv_layer(image_sobel_filtered, layer0_depth_size, layer1_depth_size, "conv-image-w0")
image_y1 = conv_layer(image_y0, layer1_depth_size, layer2_depth_size, "conv-image-w1")
image_y2 = conv_layer(image_y1, layer2_depth_size, layer3_depth_size, "conv-image-w2")

################################################################
# Depth convolution layers
#
depth_y0 = conv_layer(depth_sobel_filtered, layer0_depth_size, layer1_depth_size, "conv-depth-w0")
depth_y1 = conv_layer(depth_y0, layer1_depth_size, layer2_depth_size, "conv-depth-w1")
depth_y2 = conv_layer(depth_y1, layer2_depth_size, layer3_depth_size, "conv-depth-w2")

#################################################################################
# Inner layer 1.
#
z3 = tf.concat([tf.reshape(image_y2, [tf.shape(image_y2)[0], -1]), tf.reshape(depth_y2, [tf.shape(depth_y2)[0], -1])], 1)
z3_size = 24 + 1
w3 = init_weights([1920 + 1920, z3_size * 20], "inner-layer30-w")
b3 = init_weights([z3_size * 20], "inner-layer30-b")
z3 = tf.matmul(z3, w3) + b3
#z3 = tf.nn.dropout(z3, p_keep_conv)
z3 = tf.nn.relu(z3)
#z3 = tf.nn.dropout(z3, p_keep_conv)

w30 = init_weights([z3_size * 20, z3_size], "inner-layer31-w")
b30 = init_weights([z3_size], "inner-layer31-b")
z3 = tf.matmul(z3, w30) + b30 
z3 = tf.nn.softmax(z3)
#z3 = tf.nn.dropout(z3, p_keep_conv)

#################################################################################
# Inner layer 2.
#
def subinner_layer(input_data, image_data, depth_data, height0, wight0, height1, wight1, depth_layers, g, name):
    z = input_data[:,1:]
    z = tf.reshape(z, [-1, height0, wight0, 1])
    z = tf.reshape(tf.extract_image_patches(z, [1, 3, 3, 1], [1, 1, 1, 1], [1, 1, 1, 1], padding='SAME'), [tf.shape(z)[0], height0 * wight0, 9])

    image_z = tf.reshape(image_data, [-1, height1, wight1, depth_layers])
    image_z = tf.reshape(
            tf.extract_image_patches(image_z, [1, 3 * 4, 3 * 4, 1], [1, 4, 4, 1], [1, 1, 1, 1], padding='SAME'), 
            [tf.shape(image_data)[0], height0 * wight0, 12 * 12 * depth_layers]
        )
    depth_z = tf.reshape(depth_data, [-1, height1, wight1, depth_layers])
    depth_z = tf.reshape(
            tf.extract_image_patches(depth_z, [1, 3 * 4, 3 * 4, 1], [1, 4, 4, 1], [1, 1, 1, 1], padding='SAME'), 
            [tf.shape(depth_data)[0], height0 * wight0, 12 * 12 * depth_layers]
        )

    z = tf.concat([z, image_z, depth_z], 2)
    w = init_weights([height0 * wight0, 12 * 12 * depth_layers * 2 + 9, g], name + "-w")
    b = init_weights([height0 * wight0, g], name + "-b")
    z = tf.einsum("ijp,jpr->ijr", z, w) + b

    z = tf.reshape(z, [-1, height0 * wight0 * g])
    z = tf.concat([input_data[:,0:1], z], 1)
    z = tf.nn.relu(z)
    #z = tf.nn.dropout(z, p_keep_conv)
    return z

z2 = subinner_layer(z3, image_y1, depth_y1, layer3_height, layer3_wight, layer2_height, layer2_wight, layer2_depth_size, 64, "inner-layer20")

z2_size = 336
w20 = init_weights([24 * 64 + 1, z2_size + 1], "inner-layer21-w")
b20 = init_weights([z2_size + 1], "inner-layer21-b")
z2 = tf.matmul(z2, w20) + b20 
z2 = tf.nn.softmax(z2)
#z2 = tf.nn.dropout(z2, p_keep_conv)

#################################################################################
# Inner layer 3.
#

z1 = subinner_layer(z2, image_y0, depth_y0, layer2_height, layer2_wight, layer1_height, layer1_wight, layer1_depth_size, 32, "inner-layer10")

z1_size = 5184
w10 = init_weights([10753, z1_size + 1], "inner-layer11-w")
b10 = init_weights([z1_size + 1], "inner-layer11-b")
z1 = tf.matmul(z1, w10) + b10

z1 = tf.nn.softmax(z1, name = "z1")
z1 = tf.nn.dropout(z1, p_keep_conv)

z1_cost = tf.reduce_sum(out_classes1 * tf.log(z1 + 1e-10), reduction_indices=[1])
z2_cost = tf.reduce_sum(out_classes2 * tf.log(z2 + 1e-10), reduction_indices=[1])
z3_cost = tf.reduce_sum(out_classes3 * tf.log(z3 + 1e-10), reduction_indices=[1])

cost = tf.reduce_mean(
       - z1_cost 
       + (z2_cost * z2_cost)
       - (z3_cost * z3_cost * z3_cost),
       name = "cost")
#cost = -tf.reduce_sum((out_data * tf.log(tf.clip_by_value(z1,1e-10,1.0))), name = "cost")
#cost = -tf.reduce_sum((out_data * tf.log(z1 + 1e-10)), name = "cost")
#cost = -tf.reduce_sum((out_data * tf.log(z1))) + tf.reduce_sum((z1 * tf.log(z1)))
#cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(z3, out_data))
#cost = tf.reduce_sum(tf.pow(z1 - out_data, 2))
#train_op = tf.train.RMSPropOptimizer(0.001, 0.9).minimize(cost)
#train_op = tf.train.GradientDescentOptimizer(0.005).minimize(cost)
train_op = tf.train.AdamOptimizer(0.01).minimize(cost)

# Initilizing the variables
init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 1 }
    )
saver = tf.train.Saver()

x1, x2, x3, x4, x5, x6 = read_file("../generated-set/1491870375793-0")
myfunc = lambda t: t
vfunc = np.vectorize(myfunc)


with tf.Session(config=config) as sess:
    sess.run(init)
#    if os.path.isdir('model'):
    #saver.restore(sess, 'model/my-model')

 #   saver = tf.train.import_meta_graph('model/my-model.meta')
    saver.restore(sess, 'model/my-model')

    data1 = sess.run(depth_y0, feed_dict={image_input: x1, depth_input: x2})
    data = vfunc((data1[:,:,:,0]).reshape((data1.shape[1], data1.shape[2],1)))
    data = np.concatenate((data,data,data), 2)
    print(data1)
    #toimage(vfunc(np.transpose(x1.reshape((wight, height))))).show()
#    img = Image.frombytes("L", (data.shape[1], data.shape[0]), data.tostring())
    #img = Image.fromarray(data, 'RGB')
    plt.imshow(data)
    plt.imshow(data)
    plt.imshow(data)
    plt.imshow(data)
    plt.show()
#    img.save('pictures/my.png')
 #   img.show()

#    print(z.shape)
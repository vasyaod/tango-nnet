import tensorflow as tf
import numpy as np
import math

from graph_v5_common import *

def create_graph(wight, height, objects, p_keep_conv, init_weights, init_weights2):
    image_input = tf.placeholder(tf.float32, name = "image_input")

    ################################################################
    # Sobel filtering
    #
    sobel_x = tf.constant([[-1.0, 0.0, 1.0], [-2.0, 0.0, 2.0], [-1.0, 0.0, 1.0]], tf.float32)
    sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
    sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])

    # Sobel filter for image 
    image_filtered_x = tf.nn.conv2d(image_input, sobel_x_filter,
                            strides=[1, 1, 1, 1], padding='SAME')
    image_filtered_y = tf.nn.conv2d(image_input, sobel_y_filter,
                            strides=[1, 1, 1, 1], padding='SAME')

    image_sobel_filtered = tf.sqrt(tf.square(image_filtered_x) + tf.square(image_filtered_y), name = "image_sobel_filtered")

    ################################################################
    # Convolution layer parameters
    #
    print("Convolutional layer structure:");
    layer0 = Layer(devider = 5.0, depth_size = 1)
    print(" - layer 0", layer0.__dict__);

    layer01 = Layer(devider = 2.0, depth_size = 10, prev_layer = layer0)
    print(" - layer 0/1", layer01.__dict__);

    layer1 = Layer(devider = 2.0, depth_size = 10, prev_layer = layer01)
    print(" - layer 1", layer1.__dict__);

    layer11 = Layer(devider = 2.0, depth_size = 30, prev_layer = layer1)
    print(" - layer 1/1", layer01.__dict__);

    layer2 = Layer(devider = 2.0, depth_size = 40, prev_layer = layer11)
    print(" - layer 2", layer2.__dict__);

    layer21 = Layer(devider = 2.0, depth_size = 60, prev_layer = layer2)
    print(" - layer 1/1", layer01.__dict__);

    layer3 = Layer(devider = 2.0, depth_size = 80, prev_layer = layer21)
    print(" - layer 3", layer3.__dict__);

    ################################################################
    # Convolution defenition
    #
    def conv_layer(input_data, input_depth_size, output_depth_size, stride, name):
        w = init_weights2([4, 4, input_depth_size, output_depth_size], name)
        y = tf.nn.relu(tf.nn.conv2d(input_data, w, strides=[1, 1, 1, 1], padding='SAME'))
        y = tf.nn.max_pool(y, ksize=[1, stride, stride, 1], strides=[1, stride, stride, 1], padding='SAME')
        #y = tf.nn.dropout(0, p_keep_conv)
        return y

    ################################################################
    # Image convolution layers
    #
    image_y0 = conv_layer(image_sobel_filtered, layer0.depth_size, layer01.depth_size, stride = 2, name = "conv-image-w01")
    image_y0 = conv_layer(image_y0, layer01.depth_size, layer1.depth_size, stride = 2, name = "conv-image-w0")

    image_y1 = conv_layer(image_y0, layer1.depth_size, layer11.depth_size, stride = 2, name = "conv-image-w11")
    image_y1 = conv_layer(image_y1, layer11.depth_size, layer2.depth_size, stride = 2, name = "conv-image-w1")

    image_y2 = conv_layer(image_y1, layer2.depth_size, layer21.depth_size, stride = 2, name = "conv-image-w21")
    image_y2 = conv_layer(image_y2, layer21.depth_size, layer3.depth_size, stride = 2, name = "conv-image-w2")

    #################################################################################
    # Inner layer 1.
    #
    z3 = initial_inner_layer([image_y2], objects, 4, 6, 80, init_weights, "inner-layer3")
    z3 = tf.nn.softmax(z3, name = "z4")

    #################################################################################
    # Inner layer 2.
    #
    z2 = subinner_layer(z3, [image_y1], objects, 4, 3, layer3.height, layer3.wight, layer2.height, layer2.wight, layer2.depth_size, 64, 9, init_weights, "inner-layer20")
    z2 = tf.nn.softmax(z2, name = "z2")

    #################################################################################
    # Inner layer 3.
    #
    z1 = subinner_layer(z2, [image_y0], objects, 4, 5, layer2.height, layer2.wight, layer1.height, layer1.wight, layer1.depth_size, 64, 25, init_weights, "inner-layer10")
    z1 = tf.nn.softmax(z1, name = "z1")

    return image_input, z3, z2, z1
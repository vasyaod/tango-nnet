import tensorflow as tf
import numpy as np
import math

################################################################
# Convolution layer parameters
#
class Layer:
    def __init__(self, devider, depth_size, prev_layer=None):
        self.depth_size = depth_size
        self.prev_layer = prev_layer
        if (prev_layer):
            self.devider = prev_layer.devider * devider
            self.wight = int(math.ceil(prev_layer.wight / devider))
            self.height = int(math.ceil(prev_layer.height / devider))
        else:
            self.devider = devider
            self.wight = 1920 / devider
            self.height = 1080 / devider
        
    def size(self):
        return self.wight * self.height

def initial_inner_layer(images, objects, height, wight, depth, init_weights, name):
    bunch_size = tf.shape(images[0])[0]

    if len(images) == 1:
        z = tf.reshape(images[0], [bunch_size, -1])
    else:
        z = tf.concat(map(lambda image: tf.reshape(image, [bunch_size, -1]), images), 1)
        
    z_size = height * wight + 1
    w0 = init_weights([objects, height * wight * depth * len(images), z_size * 5], name + "-w0")
    b0 = init_weights([objects, z_size * 5], name + "-b0")
    z = tf.einsum("ij,ojk->iok", z, w0) + b0
    #z = tf.nn.dropout(z, p_keep_conv)
    z = tf.nn.relu(z)

    w1 = init_weights([objects, z_size * 5, z_size], name + "-w1")
    b1 = init_weights([objects, z_size], name + "-b1")
    z = tf.einsum("ioj,ojk->iok", z, w1) + b1
    return z

def subinner_layer(input_data, images, objects, stride, overlap, height0, wight0, height1, wight1, depth_layers, g, top, init_weights, name):
    bunch_size = tf.shape(input_data)[0]
    z = input_data[:,:,1:]

    z = tf.reshape(z, [bunch_size * objects, height0 * wight0])

    # Here very triky indecies computation
    index_part1 = tf.nn.top_k(z,top)[1]
    index_part1 = tf.reshape(index_part1, [bunch_size * objects * top])

    index_part2 = tf.range(bunch_size)
    index_part2 = tf.reshape(index_part2, [bunch_size, 1])
    index_part2 = tf.tile(index_part2, [1, objects * top])
    index_part2 = tf.reshape(index_part2, [bunch_size * objects * top])

    index_part3 = tf.range(objects)
    index_part3 = tf.reshape(index_part3, [objects, 1])
    index_part3 = tf.tile(index_part3, [1, top])
    index_part3 = tf.reshape(index_part3, [objects * top])
    index_part3 = tf.tile(index_part3, [bunch_size])

    # We have two type of indecies
    indecies_type1 = tf.stack([index_part2, index_part3, index_part1])
    indecies_type1 = tf.transpose(indecies_type1)
    indecies_type2 = tf.stack([index_part2, index_part1])
    indecies_type2 = tf.transpose(indecies_type2)
    # Indecies were builden

    # Extract "max" field form privies layer
    z = tf.reshape(z, [bunch_size * objects, height0, wight0, 1])
    z = tf.extract_image_patches(z, [1, overlap, overlap, 1], [1, 1, 1, 1], [1, 1, 1, 1], padding='SAME')
    l1 = tf.reshape(z, [bunch_size, objects, height0 * wight0, overlap * overlap])
    l1 = tf.gather_nd(l1, indecies_type1)
    l1 = tf.reshape(l1, [bunch_size, objects, top, overlap * overlap])

    # Extract "max" field form image
    def image_to_patches(image):
        image_z = tf.reshape(image, [-1, height1, wight1, depth_layers])
        image_z = tf.extract_image_patches(image_z, [1, overlap * stride, overlap * stride, 1], [1, stride, stride, 1], [1, 1, 1, 1], padding='SAME')
        image_z = tf.reshape(image_z, [bunch_size, height0 * wight0, overlap * stride * overlap * stride * depth_layers])
        l = tf.gather_nd(image_z, indecies_type2)
        l = tf.reshape(l, [bunch_size, objects, top, overlap * stride * overlap * stride * depth_layers])
        return l
    
    ls = map(image_to_patches, images)
    ls.append(l1)

    z = tf.concat(ls, 3)
    w = init_weights([objects, overlap * stride * overlap * stride * depth_layers * len(images) + overlap * overlap, g], name + "-w0")
    b = init_weights([objects, g], name + "-b0")
    z = tf.einsum("iocp,opr->icor", z, w) + b           # Here 'o' is number of objects (points)...

    z = tf.nn.relu(z)

    w = init_weights([objects, g, stride*stride], name + "-w1")
    b = init_weights([objects, stride*stride], name + "-b1")
    z = tf.einsum("icop,opr->icor", z, w) + b           # Here 'o' is number of objects (points)...
    z = tf.einsum("icop->iocp", z)                      # Transponation

    z = tf.reshape(z, [bunch_size * objects * top, stride*stride])
    z = tf.scatter_nd(indecies_type1, z, [bunch_size, objects, height0 * wight0, stride*stride])

    z = tf.reshape(z, [bunch_size, objects, height0, wight0, stride, stride])
    z = tf.transpose(z, [0, 1, 2, 4, 3, 5])
    z = tf.reshape(z, [bunch_size, objects, height0 * stride, wight0 * stride])
    z = tf.reshape(z, [bunch_size, objects, height0 * stride * wight0 * stride])
    z = z[:,:, 0:height1 * wight1]

    z = tf.concat([input_data[:,:,0:1], z], 2)
    return z

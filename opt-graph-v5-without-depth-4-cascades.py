#!/usr/bin/python

import tensorflow as tf
import math
import numpy as np
import argparse

from graph_v5_without_depth_4_cascades import *

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--number-of-objects', action='store', type=int, required=True)
args = parser.parse_args()

p_keep_conv = 1.0
objects = args.number_of_objects
wight = 1920 / 5 #384
height = 1080 / 5 #216

def init_weights(shape, name):
#    return tf.placeholder(tf.float32, shape = shape, name = name)
#    return tf.Variable(tf.random_normal(shape, stddev=0.01, dtype = tf.float32), name = name)
#    return tf.Variable(initial_value = tf.zeros(shape), name = name)
    arr = np.fromfile("model/" + name, dtype = np.float32)
    arr = arr.reshape(shape)
    c = tf.constant(value = arr, name = name)
    return c

def init_weights2(shape, name):
#    return tf.placeholder(tf.float32, shape = shape, name = name)
#    return tf.Variable(tf.random_normal(shape, stddev=0.03, dtype = tf.float32), name = name)
#    return tf.Variable(initial_value = tf.zeros(shape), name = name)
    arr = np.fromfile("model/" + name, dtype = np.float32)
    arr = arr.reshape(shape)
    c = tf.constant(value = arr, name = name)
    return c

create_graph(wight, height, objects, p_keep_conv, init_weights, init_weights2)

with tf.Session() as sess:
    tf.train.write_graph(sess.graph, 'model', 'opt-graph.pb', as_text=False)
#!/bin/bash

rm model/*
curl --output model.tar.gz http://editor.f-proj.com/projects/$1/tasks/$2/model/model.tar.gz
tar -xvzf model.tar.gz
rm model.tar.gz
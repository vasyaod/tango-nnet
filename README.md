# Cascade density reconstruction
	
## Docs

Documentation and principal of algorithm work is [docs/README.md](docs/README.md)

## Training

For training appropriate version of graph command "train-*.py" should be used.

Launch parameters (parameters can be different depends on graph version):

```
optional arguments:
  -h, --help            show this help message and exit
  -b BUTCH_SIZE, --butch-size BUTCH_SIZE
  -o NUMBER_OF_OBJECTS, --number-of-objects NUMBER_OF_OBJECTS
  -p PROJECT_ID, --project-id PROJECT_ID
  -g GENERATOR_URL, --generator-url GENERATOR_URL
  -e EPOCHS, --epochs EPOCHS
```

Invocation example for version 5:

```
./train-v5.py --butch-size=10 --number-of-objects=2 --project-id="two-fingers" --generator-url="http://generator.f-proj.com" --epoch=5
```

## Saving parameter and and pure version of graph

Parameters can be saved to separate files where each file will contain one weight matrix by

```
./save-parameters.py
```

And also some "pure" version of graph can be saved in single file /model/opt-graph.py

```
./save-parameters.py
./opt-graph-v5.py -o 2
```

## Graphs / Nets

### graph_v1.py

 - cascaded restoring of density by applying some unique function to each patch
 - only single object/density

### graph_v2.py

Features:

 - cascaded restoring of density applying multiple times single function to each patch
 - only single object/density

### graph_v3.py

Features:

 - cascaded restoring of density applying multiple times single function to each patch
 - multi objects/densities

### graph_v4.py

This is unsuccessful attempt to improve model. This file was kept here for history

### graph_v5.py

Features:

 - cascaded restoring of density applying multiple times single function to each patch
 - multi objects/densities
 - calculated only more possible cells/patches

## Feedback

For any question write to [vasiliy.vazhesov@gmail.com](vasiliy.vazhesov@gmail.com)
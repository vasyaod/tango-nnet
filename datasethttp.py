#!/usr/bin/env python

import numpy as np
import os
import urllib2
import json
import base64
import time
from multiprocessing.dummy import Pool as ThreadPool 

current_milli_time = lambda: int(round(time.time() * 1000))

pool = ThreadPool(8)

def read_file(project_id, num_of_objects, sample):

    #Image
    image_vec = np.fromstring(base64.b64decode(sample['images'][0]), dtype = np.float32)
    image_vec = image_vec.reshape(1, 1080 / 5, 1920 / 5, 1)

    #Depth
    depth_vec = np.fromstring(base64.b64decode(sample['images'][1]), dtype = np.float32)
    depth_vec = depth_vec.reshape(1, 1080 / 5, 1920 / 5, 1)

    class1_vec = np.fromstring(base64.b64decode(sample['classes'][2]), dtype = np.float32)
    class1_vec = class1_vec.reshape(1, num_of_objects, 5185)

    class2_vec = np.fromstring(base64.b64decode(sample['classes'][1]), dtype = np.float32)
    class2_vec = class2_vec.reshape(1, num_of_objects, 337)

    class3_vec = np.fromstring(base64.b64decode(sample['classes'][0]), dtype = np.float32)
    class3_vec = class3_vec.reshape(1, num_of_objects, 25)

    out_vec = np.fromstring(sample['coordinates'], dtype = np.float32, sep=' ')
    out_vec = out_vec.reshape(1, num_of_objects, 2)

    return (image_vec, depth_vec, class1_vec, class2_vec, class3_vec, out_vec)

def get_banch(project_id, num_of_objects, bunch_size, generator_url = "http://localhost:8091", cross_validation=False):

    tm = current_milli_time()

    if cross_validation:
        resource = "/generate-cross-samples"
    else:
        resource = "/generate-samples"

    response = urllib2.urlopen(generator_url + "/" + project_id + resource + "?number=" + str(bunch_size)).read()
    data = json.loads(response)
    if cross_validation and len(data['samples']) < 3:
        raise Exception("At least 3 cross validation item should be in dataset")

    tm1 = current_milli_time()
    results = pool.map(lambda sample: read_file(project_id, num_of_objects, sample), data['samples'])
    zz0, zz1, zz2, zz3, zz4, zz5 = results[0]
    
    for k in range(len(results) - 1):
        y0, y1, y2, y3, y4, y5 = results[k + 1]
        zz0 = np.concatenate((zz0, y0), 0)
        zz1 = np.concatenate((zz1, y1), 0)
        zz2 = np.concatenate((zz2, y2), 0)
        zz3 = np.concatenate((zz3, y3), 0)
        zz4 = np.concatenate((zz4, y4), 0)
        zz5 = np.concatenate((zz5, y5), 0)

    print("Data extracting time", current_milli_time() - tm, current_milli_time() - tm1)
    return zz0, zz1, zz2, zz3, zz4, zz5
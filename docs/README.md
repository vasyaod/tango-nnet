# Cascade computation of probability density for image recognizing

In this article we will consider a creation process of model of object position recognition. 
The model was developed for finding positions of different parts of body, for example, 
like fingers or eyes and so on. Let’s begin with some simple case. It will be finger as 
you can see on figure 1. In the picture green point represent a current position of the 
finger and our goal is to find coordinates of the finger. 

Inputs will have two pictures (two dimensional float arrays, resolution: 384x216 = (1920/5x1080/5)):

 - Monocrome image
 - Depth map where every point is distance to object

Output of our model can be represented by at least two way:

 - coordinates (x, y) in Cartesian coordinate system + 1 value of probability that point 
 doesn't exist
 - a mesh which can be considered a probability distribution that a point is in cell + 
 1 value of probability that point doesn't exist

The second way was chosen since loss function can be described quite easy. In our case 
output will have size 96x54 + 1 = 5185, and we have a restriction that **sum(output) = 1.0**

Figure 1:

![f1](pics/f1.png "Figure 1")

Notice: The green point on Figure 1 has the same coordinates for both images (color 
and depth). Trouble is that a depth image is shifted relatively color image but 
that is not so critical for as and we will use coordinates of a depth image. 
The green point is exacly on a finger for a depth image.

## Stage I
Let's compose several convolution layers. For our reserch is not so important complexity 
of convolution layers more over we will try to use the most siplest layers. Content of 
convolution layers is not subject of this article.

Figure 2:

![f2](pics/s1.png "Convolution scheme")

On the figure 2 we can see we have two separate input for two images, this happens since 
coordinates don't match and "depth" dimension will not be effective, I think, in this case.
Also in the picture we introduce some entity let's call it "cascade". One cascade shrinks
resolution of input images. In other convolutional networks cascade can be expressed not so 
explicitly but in any case convolution process is integration process by space and eventually 
we loose information about positions of features. And every next cascade only enhance 
losing of coordinates.

## Stage II
Actually the most straightforward way restore our probability density is to use simple 
linear function or "almost"/no linear function like inner product layer or better 
to use 2 or 3 number of them (but it can be SVM or whatever).

Figure 3:

![f3](pics/s2.png "Simplest nnet")

This approach is easy but has very big number of parameters. For learning of the NN needs 
too large dataset and time.

## Stage III
Ok. So right now our goal is to reduce number of parameters and increase speed of computation.
And for this stage we introduce "back cascade" entity. Meaning of the new entity is shown 
on figure 4:

Figure 4:

![f4](pics/s3.png "Back cascade")

Here "Output #3" and "Output #2" can be calculated from "Output #1" by operation "sum pool" 
(other words "sum pool" is convolution with kernel which consists from "one" elements)

Figure 5:

![f5](pics/s4.png "Cascade output calculation")

So far so good but we still have several issues:

 - number of parameters is still incredible large
 - we are not sure that output/result of previous cascade is valuable for evaluation of 
 current cascade

## Stage IV

Code is available for this stage: [../graph_v1.py](../graph_v1.py)

Let's continue to fight with previous issues. On this stage we will slice our images to patches. 
For example:

 - Output #3 has 24 points/values
 - we slice "Color image" and "Depth image" after cascade #2 to 24 patches (like 
 we can see on Figure 6)
 - now we can calculate every patch for Output #2 as function from corresponding patches
 - we concatinate patches of Output #2 to single image
 - and we apply softmax function

Figure 6:

![f6](pics/f6-gray.png "principles of slicing on patches")

Algorithm of caclulation is represented on fig. 7

Figure 7:

![f7](pics/s7.png "Improved cascade algorithm")

How did we improved the a algorithm depending on previous stage?! We defentrly reduced 
weight coefficients.

Size of weight matrixes for each cascade we can calculate by formulas:

 - size_of_W1 = number_of_patches * ((4*overlap)^2 * depth * input_multiplicator + overlap^2) * out1
 - size_of_W2 = number_of_patches * out1 * out2

Where:

 - input_multiplicator - since we have two "same" images for input this parameter is equal 2
 - out2 - in our case out2 is equal 16
 - out1 - output of privios layer and can have arbitrary value

Let's calculate (overlap = 1):

 - inner #3: 24*((4*overlap)^2*40*2+overlap^1)*64=1967616
 - inner #4: 24*64*16=24576
 - inner #5: 336*((4*overlap)^2*10*2+overlap^1)*64=6902784
 - inner #6: 336*64*16=344064

The result is more better but still in not ideal

## Stage V

Code is available for this stage: [../graph_v2.py](../graph_v2.py) or [../graph_v3.py](../graph_v2.py)

On this stage we make very important assumption and simplification that probabilities which are greater than 0 are concentrated in some bounded part of density. And we can shrink our weight matrixes to:

 - size_of_W1 = ((4*overlap)^2 * depth * input_multiplicator + overlap^2) * out1
 - size_of_W2 = out1 * out2

Let's calculate for (overlap = 1):

 - inner #3: ((4*overlap)^2*40*2+overlap^1)*64=81984
 - inner #4: 64*16=1024
 - inner #5: ((4*overlap)^2*10*2+overlap^1)*64=20544
 - inner #6: 64*16=1024

For (overlap = 3):

 - inner #3: ((4*overlap)^2*40*2+overlap^1)*64=737856
 - inner #4: 64*16=1024
 - inner #5: ((4*overlap)^2*10*2+overlap^1)*64=184896
 - inner #6: 64*16=1024

Those is excellent results since weight matrices will have size dozen Mb in sum.

## Stage VI

Code is available for this stage: [../graph_v5.by](../graph_v5.py)

So far so good and we need to increase speed of computation. Let's come back to assumption
that probabilities are localizated in some part of density and we don't need to computate
entire set of pathes. We can select N the most probable patches and compute probabilities only for them, other patches will have probabilities are equal 0. On fig. 8 represented final scheme of algorithm. 

Figure 8.

![f8](pics/s8.png "final scheme of algorithm")

## Feedback

For any question write to [vasiliy.vazhesov@gmail.com](vasiliy.vazhesov@gmail.com)
import tensorflow as tf
import numpy as np
import math

################################################################
# Convolution layer parameters
#
class Layer:
    def __init__(self, devider, depth_size, prev_layer=None):
        self.depth_size = depth_size
        self.prev_layer = prev_layer
        if (prev_layer):
            self.devider = prev_layer.devider * devider
            self.wight = int(math.ceil(prev_layer.wight / devider))
            self.height = int(math.ceil(prev_layer.height / devider))
        else:
            self.devider = devider
            self.wight = 1920 / devider
            self.height = 1080 / devider
        
    def size(self):
        return self.wight * self.height

def create_graph(wight, height, objects, p_keep_conv, init_weights, init_weights2):
    image_input = tf.placeholder(tf.float32, name = "image_input")
    depth_input = tf.placeholder(tf.float32, name = "depth_input")

    ################################################################
    # Sobel filtering
    #
    sobel_x = tf.constant([[-1.0, 0.0, 1.0], [-2.0, 0.0, 2.0], [-1.0, 0.0, 1.0]], tf.float32)
    sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
    sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])

    # Sobel filter for image 
    image_filtered_x = tf.nn.conv2d(image_input, sobel_x_filter,
                            strides=[1, 1, 1, 1], padding='SAME')
    image_filtered_y = tf.nn.conv2d(image_input, sobel_y_filter,
                            strides=[1, 1, 1, 1], padding='SAME')

    image_sobel_filtered = tf.sqrt(tf.square(image_filtered_x) + tf.square(image_filtered_y), name = "image_sobel_filtered")

    # Sobel image for depth
    depth_filtered_x = tf.nn.conv2d(depth_input, sobel_x_filter,
                            strides=[1, 1, 1, 1], padding='SAME')
    depth_filtered_y = tf.nn.conv2d(depth_input, sobel_y_filter,
                            strides=[1, 1, 1, 1], padding='SAME')

    depth_sobel_filtered = tf.sqrt(tf.square(depth_filtered_x) + tf.square(depth_filtered_y), name = "depth_sobel_filtered")

    ################################################################
    # Convolution layer parameters
    #
    class Layer:
        def __init__(self, devider, depth_size, prev_layer=None):
            self.depth_size = depth_size
            self.prev_layer = prev_layer
            if (prev_layer):
                self.devider = prev_layer.devider * devider
                self.wight = int(math.ceil(prev_layer.wight / devider))
                self.height = int(math.ceil(prev_layer.height / devider))
            else:
                self.devider = devider
                self.wight = 1920 / devider
                self.height = 1080 / devider
            
        def size(self):
            return self.wight * self.height

    print("Convolutional layer structure:");
    layer0 = Layer(devider = 5.0, depth_size = 1)
    print(" - layer 0", layer0.__dict__);

    layer01 = Layer(devider = 2.0, depth_size = 10, prev_layer = layer0)
    print(" - layer 0/1", layer01.__dict__);

    layer1 = Layer(devider = 2.0, depth_size = 10, prev_layer = layer01)
    print(" - layer 1", layer1.__dict__);

    layer11 = Layer(devider = 2.0, depth_size = 30, prev_layer = layer1)
    print(" - layer 1/1", layer01.__dict__);

    layer2 = Layer(devider = 2.0, depth_size = 40, prev_layer = layer11)
    print(" - layer 2", layer2.__dict__);

    layer21 = Layer(devider = 2.0, depth_size = 60, prev_layer = layer2)
    print(" - layer 1/1", layer01.__dict__);

    layer3 = Layer(devider = 2.0, depth_size = 80, prev_layer = layer21)
    print(" - layer 3", layer3.__dict__);

    ################################################################
    # Convolution defenition
    #
    def conv_layer(input_data, input_depth_size, output_depth_size, stride, name):
        w = init_weights2([4, 4, input_depth_size, output_depth_size], name)
        y = tf.nn.relu(tf.nn.conv2d(input_data, w, strides=[1, 1, 1, 1], padding='SAME'))
        y = tf.nn.max_pool(y, ksize=[1, stride, stride, 1], strides=[1, stride, stride, 1], padding='SAME')
        #y = tf.nn.dropout(0, p_keep_conv)
        return y

    ################################################################
    # Image convolution layers
    #
    image_y0 = conv_layer(image_sobel_filtered, layer0.depth_size, layer01.depth_size, stride = 2, name = "conv-image-w01")
    image_y0 = conv_layer(image_y0, layer01.depth_size, layer1.depth_size, stride = 2, name = "conv-image-w0")

    image_y1 = conv_layer(image_y0, layer1.depth_size, layer11.depth_size, stride = 2, name = "conv-image-w11")
    image_y1 = conv_layer(image_y1, layer11.depth_size, layer2.depth_size, stride = 2, name = "conv-image-w1")

    image_y2 = conv_layer(image_y1, layer2.depth_size, layer21.depth_size, stride = 2, name = "conv-image-w21")
    image_y2 = conv_layer(image_y2, layer21.depth_size, layer3.depth_size, stride = 2, name = "conv-image-w2")

    ################################################################
    # Depth convolution layers
    #
    depth_y0 = conv_layer(depth_sobel_filtered, layer0.depth_size, layer01.depth_size, stride = 2, name = "conv-depth-w01")
    depth_y0 = conv_layer(depth_y0, layer01.depth_size, layer1.depth_size, stride = 2, name = "conv-depth-w0")

    depth_y1 = conv_layer(depth_y0, layer1.depth_size, layer11.depth_size, stride = 2, name = "conv-depth-w11")
    depth_y1 = conv_layer(depth_y1, layer11.depth_size, layer2.depth_size, stride = 2, name = "conv-depth-w1")

    depth_y2 = conv_layer(depth_y1, layer2.depth_size, layer21.depth_size, stride = 2, name = "conv-depth-w21")
    depth_y2 = conv_layer(depth_y2, layer21.depth_size, layer3.depth_size, stride = 2, name = "conv-depth-w2")

    #################################################################################
    # Inner layer 1.
    #
    z3 = tf.concat([tf.reshape(image_y2, [tf.shape(image_y2)[0], -1]), tf.reshape(depth_y2, [tf.shape(depth_y2)[0], -1])], 1)
    z3_size = 24 + 1
    w3 = init_weights([objects, 1920 + 1920, z3_size * 20], "inner-layer30-w")
    b3 = init_weights([objects, z3_size * 20], "inner-layer30-b")
    z3 = tf.einsum("ij,ojk->iok", z3, w3) + b3
    #z3 = tf.nn.dropout(z3, p_keep_conv)
    z3 = tf.nn.relu(z3)

    w30 = init_weights([objects, z3_size * 20, z3_size], "inner-layer31-w")
    b30 = init_weights([objects, z3_size], "inner-layer31-b")
    z3 = tf.einsum("ioj,ojk->iok", z3, w30) + b30
    z3 = tf.nn.softmax(z3, name = "z3")
    #z3 = tf.nn.dropout(z3, p_keep_conv)

    #################################################################################
    # Inner layer 2.
    #
    def subinner_layer(input_data, image_data, depth_data, stride, overlap, height0, wight0, height1, wight1, depth_layers, g, name):
        z = input_data[:,:,1:]
        z = tf.reshape(z, [tf.shape(z)[0] * objects, height0, wight0, 1])
        z = tf.extract_image_patches(z, [1, overlap, overlap, 1], [1, 1, 1, 1], [1, 1, 1, 1], padding='SAME')
        z = tf.reshape(z, [tf.shape(input_data)[0], objects, height0 * wight0, overlap * overlap])

        image_z = tf.reshape(image_data, [-1, height1, wight1, depth_layers])
        image_z = tf.reshape(
                tf.extract_image_patches(image_z, [1, overlap * stride, overlap * stride, 1], [1, stride, stride, 1], [1, 1, 1, 1], padding='SAME'), 
                [tf.shape(depth_data)[0], 1, height0 * wight0, overlap * stride * overlap * stride * depth_layers]
            )
        image_z = tf.tile(image_z, [1, objects, 1, 1])

        depth_z = tf.reshape(depth_data, [-1, height1, wight1, depth_layers])
        depth_z = tf.reshape(
                tf.extract_image_patches(depth_z, [1, overlap * stride, overlap * stride, 1], [1, stride, stride, 1], [1, 1, 1, 1], padding='SAME'), 
                [tf.shape(depth_data)[0], 1, height0 * wight0, overlap * stride * overlap * stride * depth_layers]
            )
        depth_z = tf.tile(depth_z, [1, objects, 1, 1])

        z = tf.concat([z, image_z, depth_z], 3)
        w = init_weights([objects, overlap * stride * overlap * stride * depth_layers * 2 + overlap * overlap, g], name + "-w0")
        b = init_weights([objects, g], name + "-b0")
        z = tf.einsum("iojp,opr->ijor", z, w) + b           # Here 'o' is number of objects (points)...

        z = tf.nn.relu(z)

        w = init_weights([objects, g, stride * stride], name + "-w1")
        b = init_weights([objects, stride * stride], name + "-b1")
        z = tf.einsum("ijop,opr->ijor", z, w) + b           # Here 'o' is number of objects (points)...
        z = tf.einsum("ijop->iojp", z)          # This is transposition.... actually

        z = tf.reshape(z, [tf.shape(input_data)[0], objects, height0, wight0, stride, stride])
        z = tf.transpose(z, [0, 1, 2, 4, 3, 5])
        z = tf.reshape(z, [tf.shape(input_data)[0], objects, height0 * stride , wight0 * stride])
        z = tf.reshape(z, [tf.shape(input_data)[0], objects, height0 * stride * wight0 * stride])
        z = z[:,:, 0:height1 * wight1]

        z = tf.concat([input_data[:,:,0:1], z], 2)
        return z

    z2_size = 336
    z2 = subinner_layer(z3, image_y1, depth_y1, 4, 3, layer3.height, layer3.wight, layer2.height, layer2.wight, layer2.depth_size, 64, "inner-layer20")
    z2 = tf.nn.softmax(z2, name = "z2")
    #z2 = tf.nn.dropout(z2, p_keep_conv)

    #################################################################################
    # Inner layer 3.
    #

    z1_size = 5184
    z1 = subinner_layer(z2, image_y0, depth_y0, 4, 5, layer2.height, layer2.wight, layer1.height, layer1.wight, layer1.depth_size, 64, "inner-layer10")
    z1 = tf.nn.softmax(z1, name = "z1")

    return image_input, depth_input, z3, z2, z1
#!/usr/bin/python

import tensorflow as tf
import numpy as np
import glob
import random
import os
import math
from filereading import *
from tensorflow.python.platform import gfile
from tensorflow.python.client import timeline

init = tf.global_variables_initializer()

# Launch the graph
config = tf.ConfigProto(
        device_count = {'GPU': 0 } #, log_device_placement=True
    )

image, depth, _, _, _, _ = read_file(2, "output-test/1493625994513-13")

def class_to_coordinates(data):
    x_res = (1920.0 / 5.0 / 4.0)
    y_res = (1080.0 / 5.0 / 4.0)
    
    for i in range(np.shape(data)[0]):
        z = np.argmax(data[i])
        if z == 0:
            x = 0
            y = 0
        else:
            x = (z - 1) % int(math.ceil(x_res))
            y = (z - 1) // int(math.ceil(x_res))

        print("1--> ", np.shape(data), z, x, y, float(x) / x_res , float(y) / y_res)

with tf.Session(config=config) as sess:
    print("load graph")
    with gfile.FastGFile("model/opt-graph1.pb",'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')

    z = sess.run("z1:0", feed_dict={"image_input:0": image, "depth_input:0": depth})
    z = sess.run("z1:0", feed_dict={"image_input:0": image, "depth_input:0": depth})

    print(z)
    class_to_coordinates(z)

    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata = tf.RunMetadata()

    z = sess.run("z1:0", options=run_options, run_metadata=run_metadata, feed_dict={"image_input:0": image, "depth_input:0": depth})
    tl = timeline.Timeline(run_metadata.step_stats)
    ctf = tl.generate_chrome_trace_format()
    with open('timeline.json', 'w') as f:
        f.write(ctf)

    for step in range(10):
        tm = current_milli_time()
        z = sess.run("z1:0", feed_dict={"image_input:0": image, "depth_input:0": depth})
        print("Proccessing time: ", current_milli_time() - tm)

    z3 = sess.run("z3:0", options=run_options, run_metadata=run_metadata, feed_dict={"image_input:0": image, "depth_input:0": depth})
    print(np.shape(z3))
